# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mailfilter import models 

class NotificationAdminModel(admin.ModelAdmin):
    list_display=["id", "slug", "title", "date"]
admin.site.register(models.MailNotification, NotificationAdminModel)

class MailboxAdminModel(admin.ModelAdmin):
    list_display=["id", "name", "date"]
admin.site.register(models.MailBox, MailboxAdminModel)

class ClientFromListAdminModel(admin.ModelAdmin):
    list_display=["id", "email", "domain"]
admin.site.register(models.ClientFromList, ClientFromListAdminModel)

class MailboxMessagesAdminModel(admin.ModelAdmin):
    list_display=["id", "from_email", "to_email", "date", "subject", "is_approved"]
admin.site.register(models.MailboxMessages, MailboxMessagesAdminModel)

class MailAttachmentsAdminModel(admin.ModelAdmin):
    list_display=["id", "upload", "filename", "file_type", "file_size"]
admin.site.register(models.MailAttachments, MailAttachmentsAdminModel)

class ApprovalQueueAdminModel(admin.ModelAdmin):
    list_display=["id", "total", "unqueued"]
admin.site.register(models.ApprovalQueue, ApprovalQueueAdminModel)
