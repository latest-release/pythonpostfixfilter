from django.conf.urls import url, include
from django.contrib import admin
from mailfilter import views 

urlpatterns = [
    url(r'^/', views.MailIndexPage.as_view(), name="mail_index_page"),
    url(r'mx/notifications/', views.MXNotifications.as_view(), name="mx_notification"),
    url(r'mx/queue/', views.MXQueue.as_view(), name="mx_queue"),
    url(r'mx/attachments/', views.MXAttachments.as_view(), name="mx_attachments"),
    url(r'mx/users/', views.MXUsers.as_view(), name="mx_users"),
    url(r'mx/approved/', views.MXApproved.as_view(), name="mx_approved"),
    url(r'mx/dwn/attachments/', views.MXDownloadAttachment.as_view(), name="mx_download_attachments"),
    #url(r'mx/dwn/attachments/(?P<filename>[((?:\w|%20)+)]+)/', views.MXDownloadAttachment.as_view(), name="mx_download_attachments"),
]
