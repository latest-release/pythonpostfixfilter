# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from mailfilter import models 
from webmail import settings 
import os

class MailIndexPage(View):
    def get(self, request):
        try:
            return render(request, "mailindex.html", self.get_context_dict())
        except Exception as e:
            raise 
    
    def get_context_dict(self):
        """
        Returns dictionary of context variables
        """
        try:
            context_dict = {}
            context_dict["title"]="E-Com Webmail"
            return context_dict
        except Exception as e:
            raise 
            
class MXNotifications(View):
    def get(self, request):
        try:
            return render(request, "mxnotification.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_context_dict(self):
        """
        Returns context dicitonary.
        """
        try:
            context_dict = {}
            context_dict["title"]="Mx notifications"
            return context_dict
        except Exception as e:
            raise
            
class MXQueue(View):
    def get(self, request):
        try:
            return render(request, "mxqueue.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_context_dict(self):
        """
        Returns context dicitonary.
        """
        try:
            context_dict = {}
            context_dict["title"]="Mx queue"
            return context_dict
        except Exception as e:
            raise
            
class MXAttachments(View):
    def get(self, request):
        try:
            return render(request, "mxattachments.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_context_dict(self):
        """
        Returns context dicitonary.
        """
        try:
            context_dict = {}
            context_dict["title"]="Mx attachments"
            context_dict["mxattachments"]=self.query_all_attachments()
            return context_dict
        except Exception as e:
            raise
    
    def query_all_attachments(self):
        try:
            return models.MailAttachments.objects.all()[::-1]
        except Exception as e:
            raise
            
class MXUsers(View):
    def get(self, request):
        try:
            return render(request, "mxusers.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_context_dict(self):
        """
        Returns context dicitonary.
        """
        try:
            context_dict = {}
            context_dict["title"]="Mx users"
            return context_dict
        except Exception as e:
            raise
            
class MXApproved(View):
    def get(self, request):
        try:
            return render(request, "mxapproved.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_context_dict(self):
        """
        Returns context dicitonary.
        """
        try:
            context_dict = {}
            context_dict["title"]="Mx approved"
            return context_dict
        except Exception as e:
            raise
            
class MXDownloadAttachment(View):
    def get(self, request):
        try:
            filename = request.GET.get("filename", "")
            fileid = request.GET.get("fileid", "")
            fileto_download = self.get_download_file(fileid)
            
            if(fileto_download):
                full_path = os.path.join(settings.MEDIA_ROOT, fileto_download.upload.url)
                print full_path
                if(os.path.exists(full_path)):
                    with open(full_path, "rb") as fh:
                        response = HttpResponse(fh.read(), content_type=fileto_download.file_type)
                        response["Content-Disposition"]="inline; filename=" + os.path.basename(full_path)
                        return response 
                
            return HttpResponse(filename)
            #return render(request, "mxapproved.html", self.get_context_dict())
        except Exception as e:
            raise 
            
    def get_download_file(self, fileid):
        """
        Returns the file we want to download.
        """
        try:
            return models.MailAttachments.objects.get(id=fileid)
        except Exception as e:
            raise
            
