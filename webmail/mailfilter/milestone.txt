    1) Inbound email will be compared to a "Client From" list. 
 
    Actions: A) If the Sender has a match on the "Client From" list, that email will be put into a queue for a manager/supervisors approval. 
    Sub-Action: a) a notification will be sent to the manager indicating how many approvals are in queue 
    1) The supervisor will also have the ability to respond with the word "OK" in the subject line for a single email-immediate-approval. 
    b) a link will be generated for the supervisor to the "DASHBOARD" to approve the queues emails 
    
    B) If the Sender has a match on the "Client From" List and has an attachment of a certain TYPE (Spreadsheet, Word Doc, etc..), the attachment will be extracted from the document and stored at a designated path on the file system. 
    Sub-Action: a) a notification will be sent to the manager indicating how many approvals are in queue 
    1) The supervisor will also have the ability to respond with the word "OK" in the subject line for a single email-immediate-approval.
    b) a link will be generated for the supervisor to the "DASHBOARD" to approve the queues emails 
    C) If the Sender doesn't have a match, let the email continues as normal
