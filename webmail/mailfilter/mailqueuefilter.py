#  mailqueuefilter.py
#  
#  Copyright 2018 wangolo <wangolo@wangolo-OptiPlex-3020>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
Filtering postfix mail from queue seems not to be
something pretty easy.

Thanks to pymailq
GITHUB: https://github.com/outini/pymailq
"""

import pymailq
from pymailq import store # Fancy queue stuff 

def create_store_object():
    """
    Just creats the store and then
    returns the object
    """
    try:
        return store.PostqueueStore()
    except Exception as e:
        raise

def get_queue_summary():
    """
    Returns a list queue status.
    {u'average_mail_size': 2542,
         u'mails_by_age': {u'1_to_4_days_ago': 0,
          u'last_24h': 1,
          u'older_than_4_days': 0},
         u'max_mail_size': 2542,
         u'min_mail_size': 2542,
         u'top_errors': [(u'TLS is required, but was not offered by host hotmail-com.olc.protection.outlook.com[104.47.33.33]',
           1)],
         u'top_recipient_domains': [(u'hotmail.com', 1)],
         u'top_recipients': [(u'wangoloj@hotmail.com', 1)],
         u'top_sender_domains': [(u's8-software.com', 1)],
         u'top_senders': [(u'zulip@s8-software.com', 1)],
         u'top_status': [('deferred', 1), ('active', 0), ('hold', 0)],
         u'total_mails': 1,
         u'total_mails_size': 2542,
         u'unique_recipient_domains': 1,
         u'unique_recipients': 1,
         u'unique_sender_domains': 1,
         u'unique_senders': 1}
    """
    try:
        postfix_q = create_store_object()
        if(postfix_q):
            return postfix_q.summary()
        return 0
    except Exception as e:
        raise 

