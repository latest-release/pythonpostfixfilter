# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.db.models.signals import post_save

class MailNotification(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    slug = models.SlugField(unique=True, null=True, blank=True)
    title = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return "{}".format(self.title)
        
class MailBox(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return "{}".format(self.name)

class ClientFromList(models.Model):
    email = models.CharField(max_length=100, blank=True, null=True)
    domain = models.CharField(max_length=100, blank=True, null=True)
    
    def __str__(self):
        return "{} {}".format(self.email, self.domain)
        
class MailboxMessages(models.Model):
    mailbox = models.ForeignKey(MailBox, blank=True, null=True)
    from_email = models.CharField(max_length=100, blank=True, null=True)
    to_email = models.CharField(max_length=100, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    subject = models.CharField(max_length=100, blank=True, null=True)
    has_attachment = models.BooleanField(default=False)
    content = models.TextField(blank=True, null=True)
    is_approved = models.BooleanField(default=False)
    
    def __str__(self):
        return "{} {}".format(self.from_email, self.to_email)

class MailAttachments(models.Model):
    mail = models.ForeignKey(MailboxMessages, blank=True, null=True)
    upload = models.FileField(upload_to='attachments/%Y/%m/%d/')
    filename = models.CharField(max_length=100, blank=True)
    file_type = models.CharField(max_length=100, blank=True)
    file_size = models.CharField(max_length=100, blank=True)
    
    def __str__(self):
        return "{} {}".format(self.filename, self.file_type)
    
class ApprovalQueue(models.Model):
    message = models.ForeignKey(MailboxMessages, blank=True, null=True)
    total = models.PositiveIntegerField()
    unqueued = models.BooleanField(default=False)
    
    def __str__(self):
        return "{}".format(str(self.total))
        
