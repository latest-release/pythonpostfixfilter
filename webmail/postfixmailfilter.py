#  postfixmailfilter.py
#  
#  Copyright 2018 Wangolo Joel <wangolo@wangolo-OptiPlex-390>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import logging 
import libmilter 
import sys, time 
import os
import django 
import email
from django.core.files import File
import tempfile 
import uuid

# This example got from 
# --> https://github.com/crustymonkey/python-libmilter
# Will modify it to meet requirements.

# Details documentation
# --> https://stuffivelearned.org/doku.php?id=programming:python:python-libmilter

# Configure the settings we are going to use
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webmail.settings")
django.setup()

from mailfilter import models 
from webmail import settings 

def logging_configurations(msg):
    """
    We want to log every operations
    """
    try:
        logging.basicConfig(filename="postfixmailfilter.log", level=logging.INFO)
        logging.info(msg)
    except Exception as e:
        raise 
        
class PostfixMailFilter(libmilter.ForkMixin, libmilter.MilterProtocol):
    def __init__(self, opts=0, protos=0):
        libmilter.MilterProtocol.__init__(self, opts, protos)
        libmilter.ForkMixin.__init__(self)
        
        self.use_django=True
        self.enable_logging=True
        
        self.file_name= None 
        self.file_is_open = True
        self.tmp_filename = ""
    def log(self , msg):
        t = time.strftime('%H:%M:%S')
        #print '[%s] %s' % (t , msg)
        print '%s' % ( msg)
        sys.stdout.flush()
        
    @libmilter.noReply
    def connect(self, hostname, family, ip, port, cmdDict):
        """
        When we have a connection.
        """
        # This filenames will be deleted again
        # Create a temporary directory for processing emails.
        try:
            path = os.path.join(settings.MEDIA_ROOT, "mailprocessing")
            if(os.path.exists(path)):
                self.tmp_filename = os.path.join(path, str(uuid.uuid1()))
                self.file_name = open(self.tmp_filename, "wb")
            else:
                os.mkdir(path)
                self.tmp_filename = os.path.join(path, str(uuid.uuid1()))
                self.file_name = open(self.tmp_filename, "wb")
                
            return libmilter.CONTINUE 
        except Exception as e:
            raise
            
    @libmilter.noReply
    def helo(self, heloname):
        """
        When Helo is received.
        """
        return libmilter.CONTINUE
    
    @libmilter.noReply
    def mailFrom(self, frmAddr, cmdDict): 
        """
        This gets the MAIL FROM envelope address
        NOTE: This is where we make the comparision from.
        fromClient list.
        """
        try:
            if self.file_name:
                self.log('Return-Path: <%s>' % frmAddr)
                msg = 'Return-Path: <%s>' % frmAddr
                self.file_name.write(msg+"\n")
            else:
                print "Filename ", self.file_name, "Is None "
                 
            from_email = frmAddr.replace("<", "", len(frmAddr)).replace(">", "", len(frmAddr))
            
            if (from_email):
                try:
                    # To minimize the rate or exception let's not use get
                    # Let's just use filter.
                    email_exists = models.ClientFromList.objects.filter(email=from_email)
                except Exception as e:
                    raise 
                else:
                    if(email_exists):
                        # The email does exists
                        self.log("We have go the email")
                        self.quarantine()
                        self.log("Hold on queue")
                    else:
                        self.log("We don't have the email")
                        
                    return libmilter.CONTINUE
            return libmilter.CONTINUE
        except Exception as e:
            raise 

    @libmilter.noReply
    def rcpt(self, recip, cmdDict):
        if(self.file_name):
            x_origin = 'X-Original-To: %s' % recip
            x_delive = 'Delivered-To: %s' % recip
            
            self.file_name.write(x_origin+"\n")
            self.file_name.write(x_delive+"\n")
        else:
            self.log("None of them was found... filename not ready")
            self.log('X-Original-To: %s' % recip)
            self.log('Delivered-To: %s' % recip)
            
        return libmilter.CONTINUE
        
    @libmilter.noReply
    def header(self , key , val , cmdDict):
        self.log('%s: %s' % (key , val))
        x_headers = '%s: %s' % (key , val)
        if(self.file_name):
            self.file_name.write(x_headers+"\n")
        else:
            print "Filename is not ready ready"
        
        return libmilter.CONTINUE
        
    @libmilter.noReply
    def eoh(self, cmdDict):
        """
        When end of header is received.
        """
        return libmilter.CONTINUE
        
    def data(self, cmdDict):
        return libmilter.CONTINUE
        
    @libmilter.noReply
    def body(self, chunk, cmdDict):
        if(self.file_name):
            self.file_name.write(chunk+"\n")
        else:
            print "Filename not found in the body"
            
        return libmilter.CONTINUE
        
    def eob(self, cmdDict):
        # End of body we have received everything.
        return libmilter.CONTINUE
        
    def close(self):
        """
        A connection to the server is closed
        this is were we perform our complex processing 
        from.
        """
        try:
            # Close the file we had open for writing in.
            self.file_name.close()
            
            rcv_message = email.message_from_file(open(self.tmp_filename))
          
            if(len(rcv_message.get_payload()) >= 2):
                # We have an attachment
                
                
                # Some times the attachments are more than 1
                attachments = rcv_message.get_payload()[1:]
                
                for att in attachments:
                
                    path =  os.path.join(settings.MEDIA_ROOT, att.get_filename())
                    
                    # Create a temporary file
                    tmp_file = tempfile.TemporaryFile()
                    tmp_file.write(att.get_payload(decode=True))
                    
                    # Django file and pass the temprary file of our
                    django_file_object = File(tmp_file)
                    
                    db_attachment = models.MailAttachments()
                    db_attachment.upload.save(att.get_filename(), django_file_object, save=True)
                    db_attachment.filename=att.get_filename()
                    db_attachment.file_type=att.get_content_type()
                    db_attachment.save()
                    
                # Remove the file we created 
            os.remove(self.tmp_filename)
                
            self.log('Close called. QID: %s' % self._qid)
        except Exception as e:
            raise 
            
def runTestMilter():
    import signal , traceback
    
    # We can set our milter opts here
    opts = libmilter.SMFIF_CHGFROM | libmilter.SMFIF_ADDRCPT | libmilter.SMFIF_QUARANTINE

    # We initialize the factory we want to use (you can choose from an 
    # AsyncFactory, ForkFactory or ThreadFactory.  You must use the
    # appropriate mixin classes for your milter for Thread and Fork)
    f = libmilter.ForkFactory('inet:0.0.0.0:5000' , PostfixMailFilter , opts)
    def sigHandler(num , frame):
        f.close()
        sys.exit(0)
    signal.signal(signal.SIGINT , sigHandler)
    try:
        # run it
        f.run()
    except Exception , e:
        f.close()
        print >> sys.stderr , 'EXCEPTION OCCURED: %s' % e
        traceback.print_tb(sys.exc_traceback)
        sys.exit(3)
        
if __name__=="__main__":
    runTestMilter()
    
